# Ticket Booking System API

Esta API permite realizar la reserva de tickets para diferentes tipos de eventos. Esta desarrollada con Express, TypeScript y TypeORM.

---

## Instalación

#### 1. Clonar el repositorio

```sh
git clone https://gitlab.com/pilis1/api-events.git
```

#### 2. Instalar las dependencias

```sh
cd api-events
npm install
```

#### 3. Instalar la base de datos con docker compose

```sh
cd docker-compose/mysql
docker compose up -d
```

#### 4. Iniciar el servidor

```sh
npm run dev
```

La api estará disponible en `http://localhost:3000/api/v1`.

Para probar los endpoints puede hacerlo usando la documentación Swagger que se abre en `http://localhost:3000/docs`, o puede importar en Postman el archivo `TicketBookingSystem.json` que se encuentra en la raíz del proyecto.

---

## Entidades

En la base de datos se encuentran las siguientes entidades:

- **User**: Representa a un usuario que realiza la reserva. Un usuario puede realizar varias reservas.

- **Event**: Representa a un evento que tiene una determinada cantidad de tickets para reservar. Un evento puede tener varias reservas y un solo tipo de evento.

- **EventType**: Representa a un tipo de evento. Podría ser un recital, una obra de teatro, etc.

- **Booking**: Representa una reserva realizada por un usuario para un determinado evento.

---

## Endpoints

A continuación se detallan todos los endpoints disponibles en la API. Algunos requieren autenticación.

### Users

- **`POST /signin`**: Devuelve las credenciales para acceder al sistema.

- **`POST /signup`**: Permite registrarse en el sistema.

- **`POST /token`**: Devuelve un token de acceso.

- **`🔒 GET /users`**: Lista todos los usuarios registrados en el sistema.

- **`🔒 GET /users/:id`**: Muestra los datos de un usuario específico registrado en el sistema.

- **`🔒 POST /users`**: Crea un nuevo usuario.

- **`🔒 PUT /users/:id`**: Permite modificar los datos de un usuario.

- **`🔒 DELETE /users/:id`**: Permite eliminar un usuario específico del sistema.

### Events

- **`GET /events`**: Lista todos los eventos disponibles.

- **`GET /events/:id`**: Muestra los datos de un evento específico.

- **`🔒 POST /events`**: Crea un nuevo evento.

- **`🔒 PUT /events/:id`**: Permite modificar los datos de un evento.

- **`🔒 DELETE /events/:id`**: Permite eliminar un evento específico del sistema.

### EventTypes

- **`GET /event-types`**: Lista todos los tipos de eventos.

- **`GET /event-types/:id`**: Muestra los datos de un tipo de evento específico.

- **`🔒 POST /event-types`**: Crea un nuevo tipo de evento.

- **`🔒 PUT /event-types/:id`**: Permite modificar los datos de un tipo de evento.

- **`🔒 DELETE /event-types/:id`**: Permite eliminar un determinado tipo de evento del sistema.

### Bookings

- **`🔒 GET /bookings`**: Lista todas las reservas de tickets.

- **`🔒 GET /bookings/:id`**: Muestra los datos de una reserva específica.

- **`🔒 POST /bookings`**: Crea una nueva reserva de ticket. Requiere el id de un usuario (`id_user`) y el de un evento (`id_event`).

- **`🔒 PUT /bookings/:id`**: Permite modificar los datos de una reserva.

- **`🔒 DELETE /bookings/:id`**: Permite eliminar una reserva específica del sistema.

---

## Video demo

Aquí dejo un [link](https://www.loom.com/share/acccaf0dd28a40d29b050df78a6b1387?sid=931e8528-7c2e-475e-b946-d109f3ac6ddc) a un video demo para ver como funciona.

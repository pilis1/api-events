import { Request, Response } from 'express'
import { Event } from '../entities/Event'
import { EventType } from '../entities'

export const getEvents = async (req: Request, res: Response) => {
    try {
        const events = await Event.find({
            relations: {
                tipoEvento: true
            }
        })
        return res.json({ data: events })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const getEvent = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const event = await Event.findOne({ 
            where: { id: parseInt(id) },
            relations: {
                tipoEvento: true
            }
        })

        if(!event) return res.status(404).json({
            message: 'Event not found'
        })

        res.status(200).json({ 
            data: event
        })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const createEvent = async (req: Request, res: Response) => {
    const { nombre, descripcion, lugar, fechaHora, gps, precio, limite, tipoEvento } = req.body
    
    try {
        const eventTypeFound = await EventType.findOneBy({ id: parseInt(tipoEvento)})
        if(!eventTypeFound) return res.status(404).json({
            message: 'Event type not found'
        })

        const event = new Event()
        event.nombre = nombre
        event.descripcion = descripcion
        event.lugar = lugar
        event.fechaHora = new Date(fechaHora)
        event.gps = gps
        event.precio = precio
        event.limite = limite
        event.tipoEvento = eventTypeFound

        await event.save()

        res.status(201).json({ data: event })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params
    const { tipoEvento } = req.body
    
    try {
        const eventFound = await Event.findOneBy({ id: parseInt(id) })

        if(!eventFound) return res.status(404).json({
            message: 'Event not found'
        })

        if(tipoEvento) {
            const eventTypeFound = await EventType.findOneBy({ id: parseInt(tipoEvento)})
            if(!eventTypeFound) return res.status(404).json({
                message: 'Event type not found'
            })
        }

        await Event.update({ id: parseInt(id) }, req.body)

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const result = await Event.delete({ id: parseInt(id) })

        if(result.affected === 0) return res.status(404).json({
            message: 'Event not found'
        })

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

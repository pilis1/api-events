import { Request, Response } from 'express'
import { User } from '../entities/User'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { Booking } from '../entities'

const jwtSecret = 'somesecrettoken'
const jwtRefreshTokenSecret = 'somesecrettokenrefresh'
let refreshTokens: (string | undefined)[] = []

const createToken = (user: User) => {
    const token = jwt.sign({ id: user.id, email: user.email }, jwtSecret, { expiresIn: '20s' });
    const refreshToken = jwt.sign({ email: user.email }, jwtRefreshTokenSecret, { expiresIn: '90d' });
    
    refreshTokens.push(refreshToken);
    return {
        token,
        refreshToken
    }
}

export const refresh = async (req: Request, res: Response): Promise<any> => {
    const refreshToken = req.body.refresh
    if (!refreshToken) return res.status(401).json({
        errors: [{ message: 'Token not found' }]
    })

    if (!refreshTokens.includes(refreshToken)) return res.status(403).json({
        errors: [{ message: 'Invalid refresh token' }]
    })

    try {
        const user = jwt.verify(refreshToken, jwtRefreshTokenSecret)
        const { email } = <any> user
        const userFound = <User> await User.findOneBy({ email })
        if (!userFound) return res.status(400).json({
            message: 'The user does not exist'
        })

        const accessToken = jwt.sign({ id: userFound.id, email: userFound.email }, jwtSecret, { expiresIn: '60s' })

        res.json({ accessToken })
    } catch (error) {
        res.status(403).json({
            errors: [{ message: "Invalid token" }]
        });
    }
}

const comparePassword = async (user: User, password: string): Promise<Boolean> => {
    return await bcrypt.compare(password, user.password)
}

const createHash = async (password: string): Promise<string> => {
    const saltRounds = 10
    return await bcrypt.hash(password, saltRounds)
}

export const signUp = async (req: Request, res: Response): Promise<Response> => {
    const { email, username, password } = req.body

    if (!email || !username || !password) return res.status(400).json({ 
        message: 'Please. Send your email, username and password' 
    })

    const userFound = await User.findOne({
        where: [{ email }, { username }]
    })
    if (userFound) return res.status(400).json({
        message: 'The user already exists'
    })

    const user = new User()
    user.email = email
    user.username = username
    user.password = await createHash(password)
    
    await user.save()

    const { password: pass, ...data} = user

    return res.status(201).json({ data })
}

export const signIn = async (req: Request, res: Response): Promise<Response> => {
    const { username, password } = req.body

    if (!username || !password) return res.status(400).json({
        message: 'Please. Send your email or username and your password'
    })

    const userFound = await User
        .createQueryBuilder('user')
        .where('user.username = :username OR user.email = :email', { username, email: username })
        .addSelect('user.password')
        .getOne()
    
    if (!userFound) return res.status(400).json({
        message: 'User does not exist'
    })

    const isMatch = await comparePassword(userFound, password)
    if (!isMatch) return res.status(400).json({
        message: 'Credentials are incorrect'
    })
    
    return res.status(400).json({
        credentials: createToken(userFound)
    })
}

export const getUsers = async (req: Request, res: Response) => {
    try {
        const users = await User.find({
            relations: {
                bookings: true
            }
        })
        return res.json({ data: users })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const getUser = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const user = await User.findOne({
            where: { id: parseInt(id) },
            relations: {
                bookings: true
            }
        })

        if(!user) return res.status(404).json({
            message: 'User not found'
        })

        res.status(200).json({ 
            data: user
        })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const createUser = async (req: Request, res: Response) => {
    const { email, username, password } = req.body
    
    try {
        const user = new User()
        user.email = email
        user.username = username
        user.password = await createHash(password)

        await user.save()

        res.status(201).json({ data: user })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const updateUser = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const userFound = await User.findOneBy({ id: parseInt(id) })
        if(!userFound) return res.status(404).json({
            message: 'User not found'
        })

        if (req.body.password) {
            req.body.password = await createHash(req.body.password)
        }

        await User.update({ id: parseInt(id) }, req.body)

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            if (error.message.includes('Duplicate entry')) return res.status(400).json({
                message: 'The user already exists'
            })
            return res.status(500).json({ message: error.message })
        }
    }
}

export const deleteUser = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const result = await User.delete({ id: parseInt(id) })

        if(result.affected === 0) return res.status(404).json({
            message: 'User not found'
        })

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}
import { Request, Response } from 'express'
import { EventType } from '../entities'

export const getEventTypes= async (req: Request, res: Response) => {
    try {
        const eventTypes = await EventType.find({
            relations: {
                events: true
            }
        })
        return res.json({ data: eventTypes })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const getEventType = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const eventType = await EventType.findOne({
            where: { id: parseInt(id) },
            relations: {
                events: true
            }
        })

        if(!eventType) return res.status(404).json({
            message: 'Event type not found'
        })

        res.status(200).json({ 
            data: eventType
        })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const createEventType = async (req: Request, res: Response) => {
    const { nombre } = req.body
    
    try {
        const eventType = new EventType()
        eventType.nombre = nombre

        await eventType.save()

        res.status(201).json({ data: eventType })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const updateEventType = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const eventType = await EventType.findOneBy({ id: parseInt(id) })

        if(!eventType) return res.status(404).json({
            message: 'Event type not found'
        })

        await EventType.update({ id: parseInt(id) }, req.body)

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const deleteEventType = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const result = await EventType.delete({ id: parseInt(id) })

        if(result.affected === 0) return res.status(404).json({
            message: 'Event type not found'
        })

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

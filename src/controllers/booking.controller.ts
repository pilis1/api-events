import { Request, Response } from 'express'
import { Booking } from '../entities/Booking'
import { Event, User } from '../entities'

export const getBookings = async (req: Request, res: Response) => {
    try {
        const bookings = await Booking.find({
            relations: {
                event: true,
                user: true
            }
        })
        return res.json({ data: bookings })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const getBooking = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const booking = await Booking.findOne({ 
            where: { id: parseInt(id) },
            relations: ['event', 'user']
        })

        if(!booking) return res.status(404).json({
            message: 'Booking not found'
        })

        res.status(200).json({ 
            data: booking
        })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const createBooking = async (req: Request, res: Response) => {
    const { id_event, id_user } = req.body
    
    try {
        const eventFound = await Event.findOne({
            where: { id: parseInt(id_event) },
            relations: {
                bookings: true
            }
        })
        const userFound = await User.findOneBy({ id: parseInt(id_user) })

        if(!eventFound)  return res.status(404).json({
            message: 'Event not found'
        })

        if(!userFound)  return res.status(404).json({
            message: 'User not found'
        })
        
        const currentDate = new Date()
        currentDate.setHours(currentDate.getHours() - 2)

        if(eventFound.fechaHora < currentDate)  return res.status(404).json({
            message: 'Event finished'
        })

        const bookingsAmount = eventFound.bookings.length
        if(bookingsAmount >= eventFound.limite) return res.status(404).json({
            message: 'Booking limit reached'
        })

        const booking = new Booking()
        booking.event = eventFound
        booking.user = userFound
        booking.precio = eventFound.precio
        booking.fechaHora = eventFound.fechaHora
        booking.lugar = eventFound.lugar
        booking.gps = eventFound.gps

        await booking.save()

        res.status(201).json({ data: booking })
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const updateBooking = async (req: Request, res: Response) => {
    const { id } = req.params
    const { id_event, id_user } = req.body
    
    try {
        const booking = await Booking.findOneBy({ id: parseInt(id) })
        if(!booking) return res.status(404).json({
            message: 'Booking not found'
        })

        const eventFound = await Event.findOne({
            where: { id: parseInt(id_event) },
            relations: {
                bookings: true
            }
        })
        if(!eventFound) return res.status(404).json({
            message: 'Event not found'
        })

        const userFound = await User.findOneBy({ id: parseInt(id_user) })
        if(!userFound) return res.status(404).json({
            message: 'User not found'
        })

        const currentDate = new Date()
        currentDate.setHours(currentDate.getHours() - 2)

        if(eventFound.fechaHora < currentDate)  return res.status(404).json({
            message: 'Event finished'
        })

        const bookingsAmount = eventFound.bookings.length
        if(bookingsAmount >= eventFound.limite) return res.status(404).json({
            message: 'Booking limit reached'
        })

        const data = {
            event: eventFound,
            user: userFound,
            precio: eventFound.precio,
            fechaHora: eventFound.fechaHora,
            lugar: eventFound.lugar,
            gps: eventFound.gps
        }
        
        await Booking.update({ id: parseInt(id) }, data)

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export const deleteBooking = async (req: Request, res: Response) => {
    const { id } = req.params
    
    try {
        const result = await Booking.delete({ id: parseInt(id) })

        if(result.affected === 0) return res.status(404).json({
            message: 'Booking not found'
        })

        res.sendStatus(204)
    } catch (error) {
        if(error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

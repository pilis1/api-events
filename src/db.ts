import { DataSource } from 'typeorm'
import { User, Event, Booking, EventType } from './entities/index'

export const AppDataSource = new DataSource({
    type: 'mysql',
    host: 'localhost',
    port: 3307,
    username: 'root',
    password: 'mysql',
    database: 'ticket-system-db',
    synchronize: true,
    // logging: true,
    entities: [User, Event, EventType, Booking],
})
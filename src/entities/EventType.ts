import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { Event } from './Event'

@Entity()
export class EventType extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    
    @Column()
    nombre: string

    @OneToMany(() => Event, (event) => event.tipoEvento)
    events: Event[]
}
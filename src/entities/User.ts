import { 
    Entity, 
    BaseEntity, 
    PrimaryGeneratedColumn, 
    Column, 
    CreateDateColumn, 
    UpdateDateColumn ,
    OneToMany
} from 'typeorm'
import { Booking } from './Booking'

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    
    @Column({ unique: true })
    email: string

    @Column({ unique: true })
    username: string

    @Column({ select: false })
    password: string

    @Column({ default: true })
    active: boolean

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updateAt: Date

    @OneToMany(() => Booking, (booking) => booking.user)
    bookings: Booking[]
}
import { 
    Entity, 
    BaseEntity, 
    PrimaryGeneratedColumn, 
    Column, 
    OneToMany,
    ManyToOne
} from 'typeorm'
import { Booking } from './Booking'
import { EventType } from './EventType'

@Entity()
export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 200 })
    nombre: string

    @Column('text')
    descripcion: string

    @Column()
    lugar: string

    @Column()
    fechaHora: Date

    @Column()
    gps: string

    @Column()
    precio: number

    @Column({ default: 0 })
    limite: number

    @ManyToOne(() => EventType, (eventType) => eventType.events)
    tipoEvento: EventType

    @OneToMany(() => Booking, (booking) => booking.event)
    bookings: Booking[]
}
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm'
import { Event, User } from './index'

@Entity()
export class Booking extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @ManyToOne(() => Event, (event) => event.bookings)
    event: Event
    // id_event: number

    @ManyToOne(() => User, (user) => user.bookings)
    user: User
    // id_user: number

    @Column()
    precio: number

    @Column()
    fechaHora: Date

    @Column()
    lugar: string

    @Column()
    gps: string
}
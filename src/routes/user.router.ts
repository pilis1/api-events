import { Router } from 'express'
import passport from 'passport'
import { createUser, deleteUser, getUser, getUsers, refresh, signIn, signUp, updateUser } from '../controllers/user.controller'

const router = Router()

router.get('/users', passport.authenticate('jwt', { session: false }), getUsers)
router.get('/users/:id', passport.authenticate('jwt', { session: false }), getUser)
router.post('/users', passport.authenticate('jwt', { session: false }), createUser)
router.put('/users/:id', passport.authenticate('jwt', { session: false }), updateUser)
router.delete('/users/:id', passport.authenticate('jwt', { session: false }), deleteUser)
router.post('/signup', signUp)
router.post('/signin', signIn)
router.post('/token', refresh)

export default router
import { Router } from 'express'
import passport from 'passport'
import { 
    createEventType, 
    deleteEventType, 
    getEventType, 
    getEventTypes, 
    updateEventType 
} from '../controllers/event-type.controller'

const router = Router()

router.get('/event-types', getEventTypes)
router.get('/event-types/:id', getEventType)
router.post('/event-types', passport.authenticate('jwt', { session: false }), createEventType)
router.put('/event-types/:id', passport.authenticate('jwt', { session: false }), updateEventType)
router.delete('/event-types/:id', passport.authenticate('jwt', { session: false }), deleteEventType)

export default router
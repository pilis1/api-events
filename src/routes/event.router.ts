import { Router } from 'express'
import passport from 'passport'
import { 
    createEvent, 
    deleteEvent, 
    getEvent, 
    getEvents, 
    updateEvent 
} from '../controllers/event.controller'

const router = Router()

router.get('/events', getEvents)
router.get('/events/:id', getEvent)
router.post('/events', passport.authenticate('jwt', { session: false }), createEvent)
router.put('/events/:id', passport.authenticate('jwt', { session: false }), updateEvent)
router.delete('/events/:id', passport.authenticate('jwt', { session: false }), deleteEvent)

export default router
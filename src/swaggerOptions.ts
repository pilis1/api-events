export const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Ticket Booking System",
      version: "1.0.0",
      description: "Un sistema de reservas de tickets",
    },
    servers: [
      {
        url: "http://localhost:3000",
      },
    ],
  },
  apis: [`${__dirname}/docs/**/*.yaml`],
};

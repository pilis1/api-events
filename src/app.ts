import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import userRouter from './routes/user.router'
import eventRouter from './routes/event.router'
import bookingRouter from './routes/booking.router'
import eventTypeRouter from './routes/event-type.router'

import passportMiddleware from './middlewares/passport'
import passport from 'passport'
import passportLocal from 'passport-local'

import  swaggerUi from 'swagger-ui-express'
import swaggerJsDoc from 'swagger-jsdoc'
import { options } from './swaggerOptions'

const app = express()

app.use(morgan('dev'))
app.use(cors())
app.use(express.json())

app.use(express.urlencoded({ extended: false }))
app.use(passport.initialize())
passport.use(passportMiddleware)

const specs = swaggerJsDoc(options)
app.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(specs)
)

app.use('/api/v1', userRouter)
app.use('/api/v1', eventRouter)
app.use('/api/v1', bookingRouter)
app.use('/api/v1', eventTypeRouter)

export default app
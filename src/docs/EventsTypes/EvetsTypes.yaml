paths:
  /api/v1/event-types:
    get:
      summary: Returns a list of event types.
      tags:
        - EventTypes
      responses:
        "200":
          description: A JSON array of event types
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#components/schemas/EventType"
        "500":
          description: Internal server error
    post:
      summary: Creates a new event
      tags:
        - EventTypes
      security:
        - bearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#components/requestBodies/createEventType"
      responses:
        "200":
          description: A JSON event type object
          content:
            application/json:
              schema:
                $ref: "#components/schemas/EventType"
        "401":
          $ref: "#components/responses/UnauthorizedError"
        "500":
          description: Internal server error
  /api/v1/event-types/{eventTypeId}:
    get:
      summary: Returns a event type by ID.
      tags:
        - EventTypes
      parameters:
        - in: path
          name: eventTypeId
          required: true
          description: The event type ID that is required
          schema:
            $type: number
      responses:
        "200":
          description: A JSON event type object
          content:
            application/json:
              schema:
                $ref: "#components/schemas/EventType"
        "404":
          description: The event type was not found
        "500":
          description: Internal server error
    put:
      summary: Updates a certain event type
      tags:
        - EventTypes
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: eventTypeId
          required: true
          description: The event type ID that is required
          schema:
            $type: number
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#components/requestBodies/updateEventType"
      responses:
        "204":
          description: The event type was updated
        "401":
          $ref: "#components/responses/UnauthorizedError"
        "404":
          description: The event type was not found
        "500":
          description: Internal server error
    delete:
      summary: Deletes the event type with the specified ID
      tags:
        - EventTypes
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: eventTypeId
          required: true
          description: The event type ID that is required
          schema:
            $type: number
      responses:
        "204":
          description: The event type was deleted
        "401":
          $ref: "#components/responses/UnauthorizedError"
        "404":
          description: The event type was not found
        "500":
          description: Internal server error
components:
  schemas:
    EventType:
      type: object
      properties:
        id:
          type: integer
        nombre:
          type: string
        events:
          type: Event[]
      example:
        id: 1
        nombre: Teatro
        events: []
  requestBodies:
    createEventType:
      type: object
      properties:
        nombre:
          type: integer
          description: The name of the event type
      required:
        - nombre
      example:
        nombre: Destellos del alma
    updateEventType:
      type: object
      properties:
        nombre:
          type: integer
          description: The name of the event type
      example:
        nombre: Destellos del alma
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  responses:
    UnauthorizedError:
      description: Access token is missing or invalid
